# Docker VSCode
<a href="https://www.buymeacoffee.com/avarf" target="_blank"><img src="https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png" alt="Buy Me A Coffee" style="height: 41px !important;width: 174px !important;box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;-webkit-box-shadow: 0px 3px 2px 0px rgba(190, 190, 190, 0.5) !important;" ></a>

This repo contains Dockerfile and all the required scripts for building a docker image which contains the Microsoft VS Code, Go (version 1.15.6), Python (version 3.9), NodeJS (version 10.19.0) and NPM (version 6.14.4).

There are a number of other tools already installed that might help developers in their daily tasks including:
- git
- unzip
- wget
- curl
- dig
- nslookup
- traceroute
- telnet
- netstat
- nano
- nmap
- ping
- plumber: A tool to connect to different messaging systems including RMQ, Kafka, etc.

## VS Code extensions
The VS Code in this image already has below extensions:
- almenon.arepl
- CoenraadS.bracket-pair-colorizer
- goessner.mdmath
- golang.go
- GrapeCity.gc-excelviewer
- joaompinto.vscode-graphviz
- ms-azuretools.vscode-docker
- ms-python.python
- nervtech.mq4
- nicholishen.mql-over-cpp
- oderwat.indent-rainbow
- yzhang.markdown-all-in-one
- ms-vscode-remote.remote-containers

You can get the list of VS Code extensions as below or install/uninstall an extension:
```bash
code --list-extensions
code --install-extension [extension-name]
code --uninstall-extension [extension-name]
```

# How to run
For running the container you can use the `run.sh` script which runs the container and:
- Mounts the X11 of the host into the container: this is needed so we can get the GUI of the vscode in the host (https://unix.stackexchange.com/questions/276168/what-is-x11-exactly)
- Set DISPLAY of the container to the display of the host
- Mounts the home directory of the host into the container: this is not required and you can mount any directory that you want to but I suggest you mount the home so you can have all the extra config files for different tools such as git credentials, `.npmrc`, `.bashrc`, etc
- Uses an idle command to keep the container alive: you can just use the `code` and a path to just launch the VS Code but I prefer to have one container running and whenever I want open different paths from the cli