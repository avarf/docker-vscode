#!/bin/bash -eux

if [ "$(docker ps -q -f name=vscode)" ]; then
    echo "VS Code is running"
elif [ "$(docker ps -aq -f status=exited -f name=vscode)" ]; then
    docker start vscode
else
    docker run -d -v /tmp/.X11-unix:/tmp/.X11-unix:rw -v ~:/home/dev:rw -e DISPLAY=unix${DISPLAY} --name vscode registry.gitlab.com/avarf/docker-vscode tail -f /dev/null
fi