#!/usr/bin/env python3


import argparse
import subprocess


parser = argparse.ArgumentParser("push.py")
parser.add_argument("add", help="file/directory to add", type=str)
parser.add_argument("commit", help="commit message", type=str, default="new commit")

args = parser.parse_args()

add = args.add
commit = args.commit

print("adding "+add+" to git")
subprocess.run("git add "+add, shell=True)

print("======================================================")
print("Commit message: "+commit)
subprocess.run('git commit -m "'+commit+'"', shell=True)

print("======================================================")
print("pushing to remote")
subprocess.run("git push origin master", shell=True)