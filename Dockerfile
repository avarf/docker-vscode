FROM ubuntu:20.04

RUN apt-get update --fix-missing

RUN apt-get update && apt-get install -y wget
RUN apt-get update && apt-get install -y curl
RUN apt-get update && apt-get install -y gnupg2

# vscode dependencies
RUN apt-get update && apt-get install -y libx11-xcb1 x11-apps libice6 libsm6 libxaw7 libxft2 libxmu6 libxpm4 libxt6 xbitmaps

RUN apt-get update && apt-get install -y libc6-dev
RUN apt-get update && apt-get install -y libgtk2.0-0
RUN apt-get update && apt-get install -y libgtk-3-0
RUN apt-get update && apt-get install -y libpango-1.0-0
RUN apt-get update && apt-get install -y libcairo2
RUN apt-get update && apt-get install -y libfontconfig1
# ubuntu 18
# RUN apt-get update && apt-get install -y libgconf2-4
# ubuntu 19+
RUN apt-get update && apt-get install -y libgconf-2-4

RUN apt-get update && apt-get install -y libnss3
RUN apt-get update && apt-get install -y libasound2
RUN apt-get update && apt-get install -y libxtst6
RUN apt-get update && apt-get install -y libglib2.0-bin
RUN apt-get update && apt-get install -y libcanberra-gtk-module
RUN apt-get update && apt-get install -y libgl1-mesa-glx
RUN apt-get update && apt-get install -y build-essential
RUN apt-get update && apt-get install -y gettext
RUN apt-get update && apt-get install -y libstdc++6
RUN apt-get update && apt-get install -y git
RUN apt-get update && apt-get install -y unzip
RUN apt-get update && apt-get install -y xterm
RUN apt-get update && apt-get install -y sudo


# to solve /bin/sh: 1: add-apt-repository: not found
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends software-properties-common

# create our developer user
RUN groupadd -r dev -g 1000
RUN useradd -u 1000 -r -g dev -s /bin/bash dev
RUN echo "dev ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

WORKDIR /home/dev
USER dev

RUN sudo chown -R dev:dev /home/dev


# installing VSCode
RUN wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
RUN sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
RUN sudo apt install -y code

# for installing nslookup and dig
RUN sudo apt-get install -y dnsutils
RUN sudo apt-get install -y traceroute
RUN sudo apt-get install -y telnet
# for installing netstat
RUN sudo apt-get install -y net-tools
RUN sudo apt-get install -y nano
RUN sudo apt-get install -y nmap
RUN sudo apt-get install -y iputils-ping

# installing plumber for connecting to different messaging systems including RMQ and Kafak
RUN curl -o plumber https://github.com/batchcorp/plumber/releases/latest/download/plumber-darwin
RUN sudo chmod +x plumber
RUN sudo mv plumber /usr/local/bin/plumber

# installing Python
RUN sudo add-apt-repository ppa:deadsnakes/ppa
RUN sudo apt install -y python3.9
RUN sudo apt install -y python3-pip

# installing node
RUN sudo apt install -y nodejs
RUN sudo apt install -y npm

# installing GO
RUN wget -c https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
RUN export PATH=$PATH:/usr/local/go/bin


# installing VSCode extensions
# there is a problem that we cannot run code as root
RUN code --install-extension almenon.arepl
RUN code --install-extension CoenraadS.bracket-pair-colorizer
RUN code --install-extension goessner.mdmath
RUN code --install-extension golang.go
RUN code --install-extension GrapeCity.gc-excelviewer
RUN code --install-extension joaompinto.vscode-graphviz
RUN code --install-extension ms-azuretools.vscode-docker
RUN code --install-extension ms-python.python
RUN code --install-extension nervtech.mq4
RUN code --install-extension nicholishen.mql-over-cpp
RUN code --install-extension oderwat.indent-rainbow
RUN code --install-extension yzhang.markdown-all-in-one

# The Remote - Containers extension lets you run Visual Studio Code inside a Docker container.
RUN code --install-extension ms-vscode-remote.remote-containers

CMD ['/bin/bash /bin/cat']