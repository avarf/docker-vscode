#!/bin/bash -eux

docker build -f Dockerfile -t avarf/docker-vscode .
docker tag avarf/docker-vscode:latest registry.gitlab.com/avarf/docker-vscode:latest