Dockerizing your development environment with VS Code or how to developing inside a container


# What
In this post I will show you how to develop inside a container without the need to install anything except docker.

You can find the link to docker-hub and gitlab repo for this project at the end of this post.

# Why
But first the question is why one might want to use a dockerized version of VS Code instead of install it natively? There are many reason that you might need or want to do this:
- Standardizing the development environment among team members
- Prevent any version conflict: if you want to test something and install python 3.9 but your code started to face some problems because of the new changes in the newer version but when you run your VS Code in a container with freezed version of different languages and libraries you are safe
- If you work on different machines and you want to have a consistent development environment with the least amount of effort to set up the environment

# How
I beleive for using such a tool that is going to have access to your code especially when it comes from an unknown source you need to be sure that there is no security threat, therefore here I describe what is inside that image and at the end of the post you can find the link to the repo and be sure that everything is fine.


For running the VS Code in a container I decided to use the Ubuntu 20.04 as the base image since it is the OS that I mainly use and also a very popular OS among developers.
In the next step I update the packages and install some required tools and VS Code dependencies:
```Dockerfile
FROM ubuntu:20.04

RUN apt-get update --fix-missing

RUN apt-get update && apt-get install -y wget
RUN apt-get update && apt-get install -y curl
RUN apt-get update && apt-get install -y gnupg2

# vscode dependencies
RUN apt-get update && apt-get install -y libx11-xcb1 x11-apps libice6 libsm6 libxaw7 libxft2 libxmu6 libxpm4 libxt6 xbitmaps

RUN apt-get update && apt-get install -y libc6-dev
RUN apt-get update && apt-get install -y libgtk2.0-0
RUN apt-get update && apt-get install -y libgtk-3-0
RUN apt-get update && apt-get install -y libpango-1.0-0
RUN apt-get update && apt-get install -y libcairo2
RUN apt-get update && apt-get install -y libfontconfig1
# ubuntu 18
# RUN apt-get update && apt-get install -y libgconf2-4
# ubuntu 19+
RUN apt-get update && apt-get install -y libgconf-2-4

RUN apt-get update && apt-get install -y libnss3
RUN apt-get update && apt-get install -y libasound2
RUN apt-get update && apt-get install -y libxtst6
RUN apt-get update && apt-get install -y libglib2.0-bin
RUN apt-get update && apt-get install -y libcanberra-gtk-module
RUN apt-get update && apt-get install -y libgl1-mesa-glx
RUN apt-get update && apt-get install -y build-essential
RUN apt-get update && apt-get install -y gettext
RUN apt-get update && apt-get install -y libstdc++6
RUN apt-get update && apt-get install -y git
RUN apt-get update && apt-get install -y unzip
RUN apt-get update && apt-get install -y xterm
RUN apt-get update && apt-get install -y sudo

# to solve /bin/sh: 1: add-apt-repository: not found
RUN apt update && DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends software-properties-common
```

In the next step we will create a new user and add it to the sudoers list and we remove the password requirement for it. The reasons that we need a new user are:
- It is not safe to run a container as root
- Some applications such as VS Code need to be launched as non-root
```Dockerfile
# create our developer user
RUN groupadd -r dev -g 1000
RUN useradd -u 1000 -r -g dev -s /bin/bash dev
RUN echo "dev ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

WORKDIR /home/dev
USER dev

RUN sudo chown -R dev:dev /home/dev
```

After changing to the newly created user we start installing the VS Code:
```Dockerfile
# installing VSCode
RUN wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
RUN sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
RUN sudo apt install -y code
```

In the next step I install some of the tools that I usually need in day to day work:
```Dockerfile
# for installing nslookup and dig
RUN sudo apt-get install -y dnsutils
RUN sudo apt-get install -y traceroute
RUN sudo apt-get install -y telnet
# for installing netstat
RUN sudo apt-get install -y net-tools
RUN sudo apt-get install -y nano
RUN sudo apt-get install -y nmap
RUN sudo apt-get install -y iputils-ping

# installing plumber for connecting to different messaging systems including RMQ and Kafak
RUN curl -o plumber https://github.com/batchcorp/plumber/releases/latest/download/plumber-darwin
RUN sudo chmod +x plumber
RUN sudo mv plumber /usr/local/bin/plumber
```

After installing tools I install the languages that I need:
```Dockerfile
# installing Python
RUN sudo add-apt-repository ppa:deadsnakes/ppa
RUN sudo apt install -y python3.9
RUN sudo apt install -y python3-pip

# installing node
RUN sudo apt install -y nodejs
RUN sudo apt install -y npm

# installing GO
RUN wget -c https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz -O - | sudo tar -xz -C /usr/local
RUN export PATH=$PATH:/usr/local/go/bin
```

And in the last step I install some extensions that I need:
```Dockerfile
# installing VSCode extensions
# there is a problem that we cannot run code as root
RUN code --install-extension almenon.arepl
RUN code --install-extension CoenraadS.bracket-pair-colorizer
RUN code --install-extension goessner.mdmath
RUN code --install-extension golang.go
RUN code --install-extension GrapeCity.gc-excelviewer
RUN code --install-extension joaompinto.vscode-graphviz
RUN code --install-extension ms-azuretools.vscode-docker
RUN code --install-extension ms-python.python
RUN code --install-extension nervtech.mq4
RUN code --install-extension nicholishen.mql-over-cpp
RUN code --install-extension oderwat.indent-rainbow
RUN code --install-extension yzhang.markdown-all-in-one

# The Remote - Containers extension lets you run Visual Studio Code inside a Docker container.
RUN code --install-extension ms-vscode-remote.remote-containers
```

And now our Dockerfile is ready and we can build an image that has VS Code, different languages and tools and enables us to with the least amount of effort start coding and developing on any machine.

## How to run the container
We can run this image in two way:
- Mount a specific directory that our project is located and work with it
- Mount our home directory into the home directory of the container and benefit some extra features that comes with this such as loading different config and credentials files such as `.bashrc`, `.npmrc`, `.ssh`, `.gitconfig` and many other ones. 

I personally mount my home directory to the container and then I open a shell into the container and go to the project directory and launch the VS Code:
```bash
# start vscode container
docker run -d \
  -d \
  -v /tmp/.X11-unix:/tmp/.X11-unix:rw \
  -v ~:/home/dev:rw \
  -e DISPLAY=unix${DISPLAY} \
  --name vscode \
  avarf/docker-vscode \
  tail -f /dev/null

# start a shell into the container
docker exec -it vscode bash

# go to the project directory and launch the VS Code
cd ~/workspace/docker-vscode
code .
```

More information regarding the flags that we used for the run command:
- Mounts the X11 of the host into the container: this is needed so we can get the GUI of the vscode in the host (https://unix.stackexchange.com/questions/276168/what-is-x11-exactly)
- Set DISPLAY of the container to the display of the host
- Mounts the home directory of the host into the container: this is not required and you can mount any directory that you want to but I suggest you mount the home so you can have all the extra config files for different tools such as git credentials, `.npmrc`, `.bashrc`, etc
- Uses an idle command to keep the container alive: you can just use the `code` and a path to just launch the VS Code but I prefer to have one container running and whenever I want open different paths from the cli

Now your VS Code is ready and you can also use the internal terminal of the VS Code for easier developing.

# Where to go from here?
If you want to add any other programming language, library, tool or VS Code extension you have two ways to do it:
- Install it in the container, then create a new image from the running container
- Use my image as the base image in your Dockerfile and install the things that you need

# Links
Docker image:
registry.gitlab.com/avarf/docker-vscode

Gitlab repo:
https://gitlab.com/avarf/docker-vscode